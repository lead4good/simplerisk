FROM simplerisk/simplerisk

COPY ./000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod headers && a2enmod substitute
